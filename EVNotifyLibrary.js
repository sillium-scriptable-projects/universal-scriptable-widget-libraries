// Variables used by Scriptable.
//@ts-ignore

let DEBUG = false
const fontSizeBig = 12
const padding = 10
const fontSizeHuge = 32

async function createWidget(config) {
    DEBUG = config.debug ? true : false
    
    const log = DEBUG ? console.log.bind(console) : function () { };
    log(JSON.stringify(config, null, 2))

    //=== API Request ================================================
    let fm = FileManager.local()
    let scriptPath = module.filename
    let scriptDir = scriptPath.replace(fm.fileName(scriptPath, true), '')
    let path = fm.joinPath(scriptDir, "evnotify-cache.json")
    const apiUrlSoc = "https://app.evnotify.de/soc?akey=" + config.akey + "&token=" + config.token
    const apiUrlExtended = "https://app.evnotify.de/extended?akey=" + config.akey + "&token=" + config.token
    let data, fresh = 0
    try {
        let reqSoc = new Request(apiUrlSoc)      
        let reqExtended = new Request(apiUrlExtended)      
        try {
            data = await reqExtended.loadJSON()
            let dataSoc = await reqSoc.loadJSON()
            log("dataSoc: " + JSON.stringify(dataSoc, null, 2))

            data.soc_display = dataSoc.soc_display
            data.last_soc = dataSoc.last_soc
        
            // Write JSON to local file
            fm.writeString(path, JSON.stringify(data, null, 2))
            fresh = 1
        } catch (err) {
            // Read data from local file
            log("Exception")
            data = JSON.parse(fm.readString(path), null)
        }
    } catch(err) {
        const errorList = new ListWidget()
        errorList.addText("Error fetching JSON from https://app.evnotify.de/")
        return errorList
    }

    log("fresh: " + fresh)
    log("data: " + JSON.stringify(data, null, 2))

    // {
    //     "soc_display": 91.5,
    //     "soc_bms": 87,
    //     "last_soc": 1607963458
    //     "soh": 100,
    //     "charging": 0,
    //     "rapid_charge_port": 0,
    //     "normal_charge_port": 0,
    //     "slow_charge_port": null,
    //     "aux_battery_voltage": 14.6,
    //     "dc_battery_voltage": 358.7,
    //     "dc_battery_current": -8.5,
    //     "dc_battery_power": -3.04895,
    //     "cumulative_energy_charged": null,
    //     "cumulative_energy_discharged": null,
    //     "battery_min_temperature": 9,
    //     "battery_max_temperature": 9,
    //     "battery_inlet_temperature": 9,
    //     "external_temperature": null,
    //     "odo": null,
    //     "last_extended": 1607787548
    // }

    let showData = data
    showData.color = {}
    showData.label = {}
    showData.color.background = new Color("#555555")
    showData.logo = await getImage('evnotify.png', config.debug)
    showData.car = await getImage(config.car + '.png', config.debug)

    let dataLastUpdated = new Date(Math.max(showData.last_extended, showData.last_soc) * 1000)
    showData.dataLastUpdatedAt = dataLastUpdated.toLocaleTimeString('de-DE', { hour: "numeric", minute: "2-digit" })

    let now = new Date()
    showData.dataAge = Math.abs(now.getTime() - dataLastUpdated.getTime()) / (1000 * 60);
    showData.color.foreground = new Color("#FFFFFF")
    showData.color.foregroundCharging = new Color("#22BB22")
    if (showData.dataAge > 5) {
        showData.color.lastUpdated = new Color("#FFAAAA")
    } else {
        showData.color.lastUpdated = new Color("#FFFFFF")
    }

    var formatterRange = new Intl.NumberFormat('de-DE', {
        style: 'decimal',
        minimumFractionDigits: 0,
        maximumFractionDigits: 0
    });

    var formatterPower = new Intl.NumberFormat('de-DE', {
        style: 'decimal',
        minimumFractionDigits: 1,
        maximumFractionDigits: 1
    });

    showData.range = formatterRange.format(showData.soc_display * config.rangePerPercent) + " km"

    // if ((showData.charging == 1) && showData.soc_display < 99.0) {
    //     let missingCapacity = ((100 - showData.soc_display) / 100) * config.batteryCapacity
    //     let neededMinutes = missingCapacity / Math.abs(showData.dc_battery_power) * 60
    //     let chargeFinished = new Date(dataLastUpdated)
    //     chargeFinished.setMinutes( chargeFinished.getMinutes() + neededMinutes );
    //     showData.chargeFinishedAt = chargeFinished.toLocaleTimeString('de-DE', { hour: "numeric", minute: "2-digit" })
    // } else {
    //     showData.chargeFinishedAt = "-"
    // }

    if (showData.charging == 1) {
        showData.chargePower = formatterPower.format(Math.abs(showData.dc_battery_power)) + " kW"
    } else {
        showData.chargePower = "–"
    }

    log("showData: " + JSON.stringify(showData, null, 2))

    //=== Create Widget =====================================
    const widget = new ListWidget()
    widget.backgroundColor = showData.color.background
    widget.setPadding (padding, padding, padding, padding)

    // === Logo =====================================
    let rowLogo = addStackTo(widget, 'h')
    rowLogo.centerAlignContent()

    const evNotifyIconImg = rowLogo.addImage(showData.logo)
    evNotifyIconImg.imageSize = new Size(35, 35)

    rowLogo.addSpacer()

    const carImg = rowLogo.addImage(showData.car)
    carImg.imageSize = new Size(74, 40)

    widget.addSpacer()

    //=== Percent ===================================
    let rowPercentage = addStackTo(widget, 'h')
    rowPercentage.addSpacer()
    const percentText = rowPercentage.addText(showData.soc_display + " %")
    percentText.centerAlignText()
    percentText.font = Font.heavySystemFont(fontSizeHuge)
    percentText.textColor = (showData.charging == 1) ? showData.color.foregroundCharging : showData.color.foreground
    rowPercentage.addSpacer()

    widget.addSpacer()
    
    // === Range =====================================
    let rowRange = addStackTo(widget, 'h')

    let rowDetailsLeft = addStackTo(rowRange, 'v')
    const rangeLabel = rowDetailsLeft.addText("Range")
    rangeLabel.leftAlignText()
    rangeLabel.font = Font.regularRoundedSystemFont(fontSizeBig)
    rangeLabel.textColor = showData.color.foreground

    rowRange.addSpacer()

    let rowRangeRight = addStackTo(rowRange, 'v')
    const rangeValue = rowRangeRight.addText(showData.range)
    rangeValue.rightAlignText()
    rangeValue.font = Font.heavySystemFont(fontSizeBig)
    rangeValue.textColor = showData.color.foreground

    // === Charging =====================================
    let rowCharge = addStackTo(widget, 'h')

    let rowChargeLeft = addStackTo(rowCharge, 'v')
    const chargeLabel = rowChargeLeft.addText("Charging")
    chargeLabel.leftAlignText()
    chargeLabel.font = Font.regularRoundedSystemFont(fontSizeBig)
    chargeLabel.textColor = showData.color.foreground

    rowCharge.addSpacer()

    let rowChargeRight = addStackTo(rowCharge, 'v')
    const initialText = rowChargeRight.addText(showData.chargePower)
    initialText.rightAlignText()
    initialText.font = Font.heavySystemFont(fontSizeBig)
    initialText.textColor = showData.color.foreground

    // // === Charging =====================================
    // let rowCharge = addStackTo(widget, 'h')

    // let rowChargeLeft = addStackTo(rowCharge, 'v')
    // const chargeLabel = rowChargeLeft.addText("Charge until")
    // chargeLabel.leftAlignText()
    // chargeLabel.font = Font.regularRoundedSystemFont(fontSizeBig)
    // chargeLabel.textColor = showData.color.foreground

    // rowCharge.addSpacer()

    // let rowChargeRight = addStackTo(rowCharge, 'v')
    // const initialText = rowChargeRight.addText(showData.chargeFinishedAt)
    // initialText.rightAlignText()
    // initialText.font = Font.heavySystemFont(fontSizeBig)
    // initialText.textColor = showData.color.foreground
    
    // === Last updated =====================================
    let rowLastData = addStackTo(widget, 'h')

    let rowLastDataLeft = addStackTo(rowLastData, 'v')
    const lastDataLabel = rowLastDataLeft.addText("Updated")
    lastDataLabel.leftAlignText()
    lastDataLabel.font = Font.regularRoundedSystemFont(fontSizeBig)
    lastDataLabel.textColor = showData.color.foreground

    rowLastData.addSpacer()

    let rowLastDataRight = addStackTo(rowLastData, 'v')
    const lastDataValue = rowLastDataRight.addText(showData.dataLastUpdatedAt)
    lastDataValue.rightAlignText()
    lastDataValue.font = Font.heavySystemFont(fontSizeBig)
    lastDataValue.textColor = showData.color.lastUpdated
    
    widget.addSpacer(2)

    return widget
}

// get images from local filestore or download them once
async function getImage(image, forceDownload) {
    let fm = FileManager.local()
    let scriptPath = module.filename
    let scriptDir = scriptPath.replace(fm.fileName(scriptPath, true), '')
    let path = fm.joinPath(scriptDir, image)
    if (fm.fileExists(path) && !forceDownload) {
        return fm.readImage(path)
    } else {
        // download once
        let imageUrl
        switch (image) {
            case 'hyundaiioniq.png':
                imageUrl = "https://i.imgur.com/GXmw6Tj.png"
                break
            case 'vweup.png':
                imageUrl = "https://i.imgur.com/zNZZk7r.png"
                break
            case 'evnotify.png':
                imageUrl = "https://i.imgur.com/GK0sRd9.png"
                break
            case 'weconnect.png':
                imageUrl = "https://i.imgur.com/q8hTtOB.png"
                break
            default:
                log(`Sorry, couldn't find ${image}.`);
        }
        let iconImage = await loadImage(imageUrl)
        fm.writeImage(path, iconImage)
        return iconImage
    }
}

// helper function to download an image from a given url
async function loadImage(imgUrl) {
    const req = new Request(imgUrl)
    return await req.loadImage()
}

function addStackTo(stack, layout) {
    const newStack = stack.addStack()
    if (DEBUG) newStack.backgroundColor = new Color(randomColor(), 1.0)
    if (layout == 'h') {
        newStack.layoutHorizontally()
    } else {
        newStack.layoutVertically()
    }
    return newStack
}

const randomColor = () => {
    let color = '#';
    for (let i = 0; i < 6; i++){
       const random = Math.random();
       const bit = (random * 16) | 0;
       color += (bit).toString(16);
    }
    return color;
};

module.exports = {
    createWidget
}
