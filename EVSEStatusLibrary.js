// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: pink; icon-glyph: magic;

/**
 * Icons made by Freepik from www.flaticon.com
 */

//@ts-ignore

/**
 * Create the widget
 * @param {{widgetParameter: string, debug: string}} config widget configuration
 */

const statusDict = {
    available: {
        text: 'FREI',
        color: new Color('#55bf5b', 1.0)
    },
    occupied: {
        text: 'BESETZT',
        color: new Color('#d33c3c', 1.0)
    },
    unknown: {
        text: '???',
        color: new Color('#ced33c', 1.0)
    }
}

let DEBUG = false
const fontSizeSmall = 10
const fontSizeBig = 14

async function createWidget(config) {
    DEBUG = config.debug ? true : false
    
    const log = DEBUG ? console.log.bind(console) : function () { };
    log(JSON.stringify(config, null, 2))

    if (config.widgetParameter == null || config.widgetParameter.length == 0) {
        config.widgetParameter = 'DE*REK*E00001*001,Configure Widget'
        //config.widgetParameter = 'DE*AEG*E00001*001,Steinhofplatz links'
    }

    const params = config.widgetParameter.split(',')
    const evseId = params[0]
    const title = (params.length > 1) ? params[1] : 'LADESÄULE'

    let evseInfo = await fetchEvseInformation({evseId: evseId.toUpperCase(), debug: config.debug})
    log(JSON.stringify(evseInfo, null, 2))

    const statusIconImgFile = await getImage(evseInfo.status.toLowerCase() + '.png', config.debug)
    const locationIconImgFile = await getImage('location.png', config.debug)

    const widget = new ListWidget()
    widget.setPadding(10, 10, 10, 10)

    widget.url = evseInfo.url

    // === 1 =====================================
    const rowOne = addStackTo(widget, 'h')

    // === 1 left ===================================
    const titleSurroundingStack = addStackTo(rowOne, 'v')

    titleSurroundingStack.addSpacer()

    const titleStack = addStackTo(titleSurroundingStack, 'h')
    titleStack.size = new Size(140,0)
    const titleText = titleStack.addText(title)
    titleText.centerAlignText()
    titleText.font = Font.mediumSystemFont(fontSizeBig)

    const evseIdStack = addStackTo(titleSurroundingStack, 'h')
    evseIdStack.size = new Size(140,0)
    const evseIdText = evseIdStack.addText(evseInfo.id.toUpperCase())
    evseIdText.centerAlignText()
    evseIdText.font = Font.lightSystemFont(fontSizeSmall)

    titleSurroundingStack.addSpacer()

    // === 2 =====================================
    let rowTwo = addStackTo(widget, 'h')
    rowTwo.addSpacer(5)

    // === 2 left ===================================
    let statusIconSurroundingStack = addStackTo(rowTwo, 'v')

    statusIconSurroundingStack.addSpacer(4)

    let statusIconStack = addStackTo(statusIconSurroundingStack, 'v')
    const statusIconImg = statusIconStack.addImage(statusIconImgFile)
    statusIconImg.imageSize = new Size(50, 50)
    statusIconImg.rightAlignImage()

    statusIconSurroundingStack.addSpacer()

    // === 2 center ===================================
    rowTwo.addSpacer(5)

    // === 2 right ===================================
    let statusSurroundingStack = addStackTo(rowTwo, 'v')

    statusSurroundingStack.addSpacer(4)

    let statusStack = addStackTo(statusSurroundingStack, 'v')
    statusStack.size = new Size(80,50)
    const statusText = statusStack.addText(statusDict[evseInfo.status].text)
    statusText.font = Font.mediumSystemFont(fontSizeBig)
    statusText.textColor = statusDict[evseInfo.status].color
    // statusStack.addSpacer(9)
    // const sinceLabelText = statusStack.addText("SEIT CA.")
    // sinceLabelText.font = Font.lightSystemFont(fontSizeSmall)
    // const sinceText = statusStack.addText(lastChange)
    // sinceText.font = Font.mediumSystemFont(fontSizeBig)

    statusSurroundingStack.addSpacer()

    //rowTwo.addSpacer()

    // === 3 =====================================
    let rowThree = addStackTo(widget, 'h')
    rowThree.addSpacer(5)

    // === 3 left ===================================
    let mapIconSurroundingStack = addStackTo(rowThree, 'v')

    mapIconSurroundingStack.addSpacer(6)

    let mapIconStack = addStackTo(mapIconSurroundingStack, 'v')
    const locationImg = mapIconStack.addImage(locationIconImgFile)
    locationImg.imageSize = new Size(16, 16)

    mapIconSurroundingStack.addSpacer()

    rowThree.addSpacer(6)

    // === 3 right ===================================
    let addressSurroundingStack = addStackTo(rowThree, 'v')

    addressSurroundingStack.addSpacer(4)

    const addressStack = addStackTo(addressSurroundingStack, 'v')
    const streetText = addressStack.addText(evseInfo.address[0])
    streetText.font = Font.lightSystemFont(fontSizeSmall)
    const cityText = addressStack.addText(evseInfo.address[1])
    cityText.font = Font.lightSystemFont(fontSizeSmall)

    addressSurroundingStack.addSpacer()

    //rowThree.addSpacer()

    return widget
}

function addStackTo(stack, layout) {
    const newStack = stack.addStack()
    if (DEBUG) newStack.backgroundColor = new Color(randomColor(), 1.0)
    if (layout == 'h') {
        newStack.layoutHorizontally()
    } else {
        newStack.layoutVertically()
    }
    return newStack
}

const randomColor = () => {
    let color = '#';
    for (let i = 0; i < 6; i++){
       const random = Math.random();
       const bit = (random * 16) | 0;
       color += (bit).toString(16);
    };
    return color;
 };

/**
 * Fetch HTML from chargecloud.de and extract information into JavaScript object
 * @param {{evseId: string, debug: string}} config EVSE input configuration
 */
async function fetchEvseInformation(config) {
    let url
    url = 'https://app.chargecloud.de/en/chargeme/status/' + config.evseId

    let req = new Request(url)
    let html = await req.loadString()

    // RegExr: regexr.com/5eskj
    let tableRegExp = new RegExp("<div id=\"location\"(?:.*?(?=<table))<table(?:[^>]*)>(.*?(?=<\/table>))", "s")
    let tableMatch = html.match(tableRegExp)
    let table = tableMatch[1]

    // RegExr: regexr.com/5eskm
    // Remove newlines
    let whiteSpaceRegExp = new RegExp(">\n+", "gms")
    table = table.replace(whiteSpaceRegExp, ">")

    // Remove multiple whitespaces
    whiteSpaceRegExp = new RegExp("[ ][ ]*", "gms")
    table = table.replace(whiteSpaceRegExp, " ")

    // Remove whitespaces between HTML elements
    whiteSpaceRegExp = new RegExp("[ ]<", "gms")
    table = table.replace(whiteSpaceRegExp, "<")
    whiteSpaceRegExp = new RegExp(">[ ]", "gms")
    table = table.replace(whiteSpaceRegExp, ">")

    // Split at </tr><tr>, replace <br> in address with comma, replace all other HTML tags
    let parts = table.split("</tr><tr>").map(part => part.replace(/(<br>)/ig, ',').replace(/(<([^>]+)>)/ig, '').trim())

    let evseInfo = {
        url: url
    }

    parts.forEach(part => {
        const tuple = part.split(/(.+):(.+)/)
        const key = tuple[1].toLowerCase()
        const value = tuple[2]
        evseInfo[key] = value
    })

    let addressParts = evseInfo.address.split(",(")
    evseInfo.address = addressParts[0].split(',')
    let coordinates = addressParts[1].replace(')', '').replace(', ', ',').split(',')
    evseInfo.location = {
        lat: coordinates[0],
        lon: coordinates[1]
    }

    let chargepointParts = evseInfo.chargepoint.split(' ')
    evseInfo.id = chargepointParts[0]
    evseInfo.currentType = chargepointParts[1].replace(/[\(\)]/g, '');
    delete evseInfo.chargepoint

    return evseInfo
}

// get images from local filestore or download them once
// get images from local filestore or download them once
async function getImage(image, forceDownload) {
    let fm = FileManager.local()
    let scriptPath = module.filename
    let scriptDir = scriptPath.replace(fm.fileName(scriptPath, true), '')
    let path = fm.joinPath(scriptDir, image)
    if (fm.fileExists(path) && !forceDownload) {
        return fm.readImage(path)
    } else {
        // download once
        let imageUrl
        switch (image) {
            case 'location.png':
                imageUrl = "https://i.imgur.com/cTPwkdd.png"
                break
            case 'occupied.png':
                imageUrl = "https://i.imgur.com/WTzjRVw.png" // red charging station
                // imageUrl = "https://i.imgur.com/teykfb7.png" // red car
                break
            case 'available.png':
                imageUrl = "https://i.imgur.com/GiLcyeF.png"
                break
            case 'unknown.png':
                imageUrl = "https://i.imgur.com/IioQPty.png"
                break
            default:
                console.log(`Sorry, couldn't find ${image}.`);
        }
        let iconImage = await loadImage(imageUrl)
        fm.writeImage(path, iconImage)
        return iconImage
    }
}

// helper function to download an image from a given url
async function loadImage(imgUrl) {
    const req = new Request(imgUrl)
    return await req.loadImage()
}

module.exports = {
    createWidget
}
